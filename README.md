# Contact ajax

The Contact Ajax module can be used to send all the contact forms on the
website using ajax.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/contact_ajax).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/contact_ajax).

## Contents of this file

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers

## Requirements

This module requires no modules outside of Drupal core.

## Recommended modules

- [Contact Storage](https://www.drupal.org/project/contact_storage)

## Installation

- Install as you would normally install a contributed Drupal module. Visit:
  [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
  for further information.

## Configuration

The module has no menu settings.
When enabled the module all the contact forms listed in
`admin/structure/contact` have two more settings

- enable ajax : the form will be submitted without reloading the page
- load node : the node referenced by this field will be loaded in place of the
  form.

## Maintainers

- ziomizar - [ziomizar](https://www.drupal.org/u/ziomizar)
